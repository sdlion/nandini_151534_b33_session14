<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery-1.7.2.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Birthday date</h2>
    <form>
        <div class="form-group">
            <label for="Name">name:</label>

            <input type="name" class="form-control" id="name" placeholder="Enter name">
        </div>
        <div class="form-group">
            <label for="date">Birth date:</label>
            <input type="date" class="form-control" id="date" placeholder="Enter birth date">
        </div>

        <button type="submit" class="btn btn-default">Add</button>
        <button type="submit" class="btn btn-default">Save</button>
    </form>
</div>

</body>
</html>

