<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery-1.7.2.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>gender</h2>
    <form>
        <div class="form-group">
            <label for="Name">name:</label>

            <input type="name" class="form-control" id="name" placeholder="Enter name">
        </div>
        <div class="form-group"> <label for="Select option">select option:</label>
            <div class="radio radio-info">

                <input type="radio" name="survey" id="Radios1" value="Yes">
                <label>

                    Male
                </label>
            </div>
            <div class="radio radio-info">
                <input type="radio" name="survey" id="Radios2" value="No">
                <label>
                    Female
                </label>
            </div>
            <div class="radio disabled">
                <input type="radio" name="survey" id="Radios3" value="Notsure" disabled>
                <label>
                    Others
                </label>
            </div>

        </div>

        <button type="submit" class="btn btn-default">Add</button>
        <button type="submit" class="btn btn-default">Save</button>
    </form>
</div>

</body>
</html>

