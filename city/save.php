<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery-1.7.2.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Hobbies</h2>
    <form>
        <div class="form-group">
            <label for="name">name:</label>

            <input type="name" class="form-control" id="name" placeholder="Enter name">
        </div>
        <div class="dropdown">
            <select><option value="select option">select option</option>
                <option value="volvo">Volvo</option>
                <option value="saab">Saab</option>
                <option value="opel">Opel</option>
                <option value="audi">Audi</option>
            </select>

        </div>
    </form>
</div>

</body>
</html>